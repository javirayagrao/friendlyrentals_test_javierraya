﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kata.Classes
{
    public class StringCalculator
    {
        public int Add(string numbers)
        {
            if (string.IsNullOrWhiteSpace(numbers))
                return 0;

            IEnumerable<int> values = null;

            //optional delimiter
            if (numbers[0] == '/')
            {
                //the delimiter will always starts at [2]   
                string delimiter = numbers.Substring(2, numbers.IndexOf('\n') - 2);
                var parts = numbers.Split(new string[] { "\n", delimiter }, StringSplitOptions.RemoveEmptyEntries);
                values = parts.Skip(1).Select(s => int.Parse(s));
            }
            else
            {
                var parts = numbers.Split(new char[] { ',', '\n' }, StringSplitOptions.RemoveEmptyEntries).ToList();
                values = parts.Select(s => int.Parse(s));
            }

            return Sum(values);
        }



        private int Sum(IEnumerable<int> values)
        {
            var negatives = values.Where(w => w < 0).ToList();
            if (negatives.Any())
            {
                string strNegatives = string.Empty;
                negatives.ForEach(f => strNegatives += f + ", ");
                string message = string.Format("Negatives not allowed: {0}", strNegatives.Trim(' ',','));
                Console.WriteLine(message);
                throw new NegativeNumbersException(message);
            }
            else
            {
                return values.Sum();
            }
        }


    }
}
