﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Kata.Classes;

namespace Kata
{
    [TestClass]
    public class KataTests
    {

        public StringCalculator calc { get; set; }

        [TestInitialize]
        public void SetupStringCalculator()
        {
            if (calc == null)
            {
                calc = new StringCalculator();
            }
        }


        [TestCategory("FifthRequirement")]
        [TestMethod]
        public void Test5StringBlank()
        {
            Assert.AreEqual(calc.Add(""), 0);
        }




        [TestCategory("FifthRequirement")]
        [TestMethod]
        public void Test5JustOneNumberNoDelimiter()
        {
            Assert.AreEqual(calc.Add("1"), 1);
        }

        [TestCategory("FifthRequirement")]
        [TestMethod]
        public void Test5TwoNumbersNoDelimiter()
        {
            Assert.AreEqual(calc.Add("1\n2,3"), 6);
        }

        [TestCategory("FifthRequirement")]
        [TestMethod]
        public void Test5MoreThanTwoNumbersNoDelimiter()
        {
            Assert.AreEqual(calc.Add("1\n2,3\n4,5"), 15);
        }




        [ExpectedException(typeof(NegativeNumbersException))]
        [TestCategory("FifthRequirement")]
        [TestMethod]
        public void Test5JustOneNumberNoDelimiterNegative()
        {
            Assert.AreEqual(calc.Add("-1"), 1);
        }

        [ExpectedException(typeof(NegativeNumbersException))]
        [TestCategory("FifthRequirement")]
        [TestMethod]
        public void Test5TwoNumbersNoDelimiterNegative()
        {
            Assert.AreEqual(calc.Add("1\n-2,3"), 6);
        }

        [ExpectedException(typeof(NegativeNumbersException))]
        [TestCategory("FifthRequirement")]
        [TestMethod]
        public void Test5MoreThanTwoNumbersNoDelimiterNegative()
        {
            Assert.AreEqual(calc.Add("1\n-2,-3\n4,5"), 15);
        }




        [TestCategory("FifthRequirement")]
        [TestMethod]
        public void Test5JustOneNumberDelimiter()
        {
            Assert.AreEqual(calc.Add("//@\n1"), 1);
        }

        [TestCategory("FifthRequirement")]
        [TestMethod]
        public void Test5TwoNumbersDelimiter()
        {
            Assert.AreEqual(calc.Add("//$\n1\n2$3"), 6);
        }

        [TestCategory("FifthRequirement")]
        [TestMethod]
        public void Test5MoreThanTwoNumbersDelimiter()
        {
            Assert.AreEqual(calc.Add("//$\n1\n2$3\n4$5"), 15);
        }

        [TestCategory("FifthRequirement")]
        [TestMethod]
        public void Test5LongDelimiterStringTwoNumbers()
        {
            Assert.AreEqual(calc.Add("//--\n1\n2--3"), 6);
        }

        [TestCategory("FifthRequirement")]
        [TestMethod]
        public void Test5LongDelimiterStringMoreThanTwoNumbers()
        {
            Assert.AreEqual(calc.Add("//--\n1\n2--3\n4--5"), 15);
        }




        [ExpectedException(typeof(NegativeNumbersException))]
        [TestCategory("FifthRequirement")]
        [TestMethod]
        public void Test5JustOneNumberDelimiterNegative()
        {
            Assert.AreEqual(calc.Add("//@\n-1"), -1);
        }

        [ExpectedException(typeof(NegativeNumbersException))]
        [TestCategory("FifthRequirement")]
        [TestMethod]
        public void Test5TwoNumbersDelimiterNegative()
        {
            Assert.AreEqual(calc.Add("//$\n1\n-2$3"), 6);
        }

        [ExpectedException(typeof(NegativeNumbersException))]
        [TestCategory("FifthRequirement")]
        [TestMethod]
        public void Test5MoreThanTwoNumbersDelimiterNegative()
        {
            Assert.AreEqual(calc.Add("//$\n1\n2$-3\n4$5"), 15);
        }

        [ExpectedException(typeof(NegativeNumbersException))]
        [TestCategory("FifthRequirement")]
        [TestMethod]
        public void Test5LongDelimiterStringTwoNumbersNegative()
        {
            Assert.AreEqual(calc.Add("//--\n-1\n-2--3"), 6);
        }

        [ExpectedException(typeof(NegativeNumbersException))]
        [TestCategory("FifthRequirement")]
        [TestMethod]
        public void Test5LongDelimiterStringMoreThanTwoNumbersNegative()
        {
            Assert.AreEqual(calc.Add("//--\n1\n2---3\n4---5"), 15);
        }








    }

}
